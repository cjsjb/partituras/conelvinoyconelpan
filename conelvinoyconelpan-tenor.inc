\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 6/8
		\clef "treble_8"
		\key b \minor

		R2.*4  |
%% 5
		r4 fis 8 fis 4 fis 8  |
		e 4 d 8 ~ d cis d  |
		fis 8 b, ~ b, 2  |
		R2.  |
		r4 b 8 b 4 b 8  |
%% 10
		a 4 g 8 fis 4 g 8  |
		a 2. ~  |
		a 2.  |
		r8 r b b 4 b 8  |
		a 4 g -\staccato fis 8 e  |
%% 15
		fis 4. fis 4 e 8  |
		d 4. r  |
		g 4. g ~  |
		g 8 r r g 4 fis 8  |
		e 2. ~  |
%% 20
		e 2.  |
		r8 r fis fis 4 fis 8  |
		e 4 d 8 ~ d cis d  |
		fis 8 b, ~ b, 2  |
		R2.  |
%% 25
		r8 r b b 4 b 8  |
		a 4 g 8 fis 4 g 8  |
		a 2. ~  |
		a 2.  |
		r8 r b b 4 b 8  |
%% 30
		a 4 g -\staccato fis 8 e  |
		fis 4. fis 4 e 8  |
		d 4. r8 b, b,  |
		g 4 fis 8 g 4 fis 8  |
		g 4. g  |
%% 35
		a 2. ~  |
		a 4. r8 r fis  |
		a 2.  |
		r8 a a b 4 a 8  |
		b 4 a 8 a 4. ~  |
%% 40
		a 4 r8 a 4 b 8  |
		g 4. g ~  |
		g 8 r r g fis d  |
		e 2. ~  |
		e 4. r  |
%% 45
		r8 r d' d' 4.  |
		r8 d' d' d' 4 cis' 8  |
		d' 4 cis' 8 cis' 4. ~  |
		cis' 4 r8 cis' 4 d' 8  |
		b 4 a 8 b 4 a 8  |
%% 50
		b 4 a 8 g 4 -\staccato d 8  |
		a 4 g 8 g 4. ( ~  |
		g 4. ~ g 8 fis g  |
		a 2. )  |
		R2.*3  |
		\bar "|."
	}

	\new Lyrics\lyricsto "tenor" {
		Con nues -- tras ma -- nos ya can -- sa -- das, __
		y nues -- tros pies de tan -- "to an" -- dar, __
		ve -- ni -- mos hoy a pre -- sen -- tar -- nos, Se -- ñor,
		an -- te tu al -- tar. __

		Es nues -- "tra o" -- fren -- da nues -- tro can -- to, __
		u -- "na o" -- ra -- ción u -- ni -- ver -- sal, __
		que te que -- re -- mos o -- fre -- cer hoy, Se -- ñor,
		pa -- ra que nos pue -- das per -- do -- nar. __

		"Y a" -- sí, con el vi -- "no y" con el pan, __
		nues -- tras vi -- das __ pue -- das cam -- biar. __
		"Y a" -- sí, con el vi -- "no y" con el pan, __
		Cris -- tos nue -- vos sea -- mos pa -- ra la hu -- ma -- ni -- dad. __
	}

>>
